<?php 
	
	$nilai = [72, 65, 73, 78, 75, 74, 90, 81, 87, 65, 55, 69, 72, 78, 79, 91, 100, 40, 67, 77 ,86];

	// Rata-rata nilai
	echo "Rata-rata : ";
	echo array_sum($nilai)/count($nilai);
	echo "<br>";

	// Mendapatkan 7 nilai tertinggi

	// mengurutkan array descending
	rsort($nilai);
	echo "7 Nilai tertinggi : ";
	for ($i=0; $i < 7; $i++) { 
		echo $nilai[$i];
		echo " ";
	}
	echo "<br>";

	// 7 nilai terendah

	// menugurutkan array ascending
	sort($nilai);
	echo "7 Nilai terendah : ";
	for ($i=0; $i < 7; $i++) { 
		echo $nilai[$i];
		echo " ";
	}

 ?>