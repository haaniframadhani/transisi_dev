<?php 

function jumlahHurufKecil($string)
{
	$sum = 0;
	$arrayChar = str_split($string);

	for ($i=0; $i < count($arrayChar); $i++) 
	{ 
		// Nilai ASCII untuk huruf kecil dimulai dari 97 sampai 122
		if (ord($arrayChar[$i]) > 96 && ord($arrayChar[$i]) < 123) 
		{
			$sum++;
		}
	}
	return $sum;
}

$string = "TranSISI";
echo '"'.$string.'" mengandung '.jumlahHurufKecil($string).' buah huruf kecil.';

 ?>