<?php 

function mainKoma($string)
{
	$array = explode(' ', $string);
	
	//Unigram 
	$unigram = implode(', ', $array);
	echo 'Unigram : '.$unigram;

	// Bigram
	$bigram = $array[0];
	for ($i=1; $i < count($array); $i++) { 
		if ( $i%2 == 0 ) {
			$bigram = $bigram.', '.$array[$i];
		} else {
			$bigram = $bigram.' '.$array[$i];
		}
	}
	echo '<br>';
	echo 'Bigram : '.$bigram;

	// Trigram
	$trigram = $array[0];
	for ($i=1; $i < count($array); $i++) { 
		if ( $i%3 == 0 ) {
			$trigram = $trigram.', '.$array[$i];
		} else {
			$trigram = $trigram.' '.$array[$i];
		}
	}
	echo '<br>';
	echo 'Trigram : '.$trigram;
}


// Panggil fungsi
mainKoma('Jakarta adalah ibukota negara Republik Indonesia');



?>
