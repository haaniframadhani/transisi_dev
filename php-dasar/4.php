<?php 

function blackWhite ()
{	
	$num = 1;
	echo '<table cellpadding=8>';
	for ($i=0; $i < 8; $i++) { 
		echo '<tr>';
		for ($j=0; $j < 8; $j++) { 
			// Jika merupakan kelipatan 4 atau kelipatan 3 maka cell berwarna putih, selain itu cell berwarna hitam
			if ($num%4 == 0 || $num%3 == 0) {
				echo '<td>'.$num++.'</td>';
			} else {
				echo '<td style="background-color:black; color:white">'.$num++.'</td>';
			}
		}
		echo '</tr>';
	}
	echo '</table>';
}

// Panggil fungsi 	
blackWhite()

?>
