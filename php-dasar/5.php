<?php 

// Enkripsi dengan pola : 1 -2 3 -4 5 -6 ...
function enkripsi($string)
{
	$arrayChar = str_split($string);

	for ($i=1; $i <= count($arrayChar); $i++) { 
		// Jika kelipatan 2 maka tandanya -, jika bukan kelipatan 2 maka tandanya +
		if($i%2 == 0 ) {
			echo chr( ord($arrayChar[$i-1])-$i );
		} else {
			echo chr( ord($arrayChar[$i-1])+$i );
		}
	}

}

enkripsi('DFHKNQ');


?>