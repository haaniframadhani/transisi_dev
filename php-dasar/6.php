<?php 

function cari($string)
{
	$array = [
				['f','g','h','i'],
				['j','k','p','q'],
				['r','s','t','u']
			];

	$chars = str_split($string);
	$currentIdx = 0;

	// Cari apakah karakter pertama ada dalam array
	for ($i=0; $i < count($array); $i++) { 
		if (in_array($chars[$currentIdx], $array[$i])) {
			$row = $i;
			$col = array_search($chars[$currentIdx], $array[$i]);
			$currentIdx++;
			$flag = true;
			break;
		} else {
			$flag = false;
		}
	}

	// jika karakter pertama ditemukan, maka lanjut pencarian posisi karakter berikutnya dalam array
	// jarak dari baris dan kolom sebelumnya yang memungkinkan untuk membentuk kata yang benar adalah (0,1) , (0,-1), (1,0), (-1,0)
	while ($flag && $currentIdx<count($chars)) {
		for ($i=0; $i < count($array); $i++) { 
			if (in_array($chars[$currentIdx], $array[$i])) {
				if ( abs( $row - $i) == 0 ) {
					$newCol = array_search($chars[$currentIdx], $array[$i]);
					if ( abs( $col - $newCol ) == 1 ) {
						$row = $i;
						$col = $newCol;
						$currentIdx++;
						$flag = true;
						break;
					} else {
						$flag = false;
						break;
					}
				} else if ( abs( $row - $i) == 1 ) {
					$newCol = array_search($chars[$currentIdx], $array[$i]);
					if ( abs( $col - $newCol ) == 0 ) {
						$row = $i;
						$col = $newCol;
						$currentIdx++;
						$flag = true;
						break;
					} else {
						$flag = false;
						break;
					}
				} else {
					$flag = false;
					break;
				}
			} else {
				$flag = false;
			}
		}
	}
	
	// tampilkan hasil	
	if($flag) {
		echo 'true';
	} else {
		echo 'false';
	}
	echo '<br>';
}

cari('fghi');

cari('fghp');

cari('fjrstp');

cari('fjktu');

 ?>