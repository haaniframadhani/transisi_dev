<?php

namespace Modules\Transisi\Entities;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $fillable = [
        'name', 'email', 'company_id'
    ];

}
