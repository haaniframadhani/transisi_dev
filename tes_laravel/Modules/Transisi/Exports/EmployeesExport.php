<?php

namespace Modules\Transisi\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Modules\Transisi\Entities\Employee;

class EmployeesExport implements FromQuery
{
    use Exportable;


    public function __construct(int $company_id)
    {
        $this->company = $company_id;
    }

    public function query()
    {
        return Employee::query()->where('company',$this->company);
    }
}
