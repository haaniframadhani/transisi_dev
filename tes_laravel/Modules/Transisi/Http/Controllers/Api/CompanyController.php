<?php

namespace Modules\Transisi\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;

use Modules\Transisi\Repositories\CompanyRepository;

use Modules\Transisi\Http\Requests\StoreCompanyRequest;
use Modules\Transisi\Http\Requests\UpdateCompanyRequest;

class CompanyController extends Controller
{

    private $companyRepository;

    public function __construct()
    {
        $this->companyRepository = new CompanyRepository();
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $companies = $this->companyRepository->fetch();
        return response()
                ->json([
                    'success' => true,
                    'data' => $companies,
                ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(StoreCompanyRequest $request)
    {
        $logo = time()."_".$request->file('logo')->getClientOriginalName();
        Storage::putFileAs('company', $request->file('logo'), $logo);

        $company = [
            'name' => $request->name,
            'email' => $request->email,
            'logo' => $logo,
            'website' => $request->website
        ];

        //store
        $this->companyRepository->insert($company);

        return response()
        ->json([
            'success' => true,
            'data' => $company,
        ]);
     
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $company = $this->companyRepository->find($id);
        if($company) {
            return response()->json([
                    'success' => true,
                    'data' => $company,
                ]);
        } else {
            return response()->json([
                'success' => false,
                'data'    => $company
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        $company = [
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
        ];

        if ($request->hasFile('logo')) {
            $this->validate($request, [
                'logo' => 'file|image|mimes:jpeg,png,jpg|max:2000|dimensions:min_width=100,min_height=100',
            ]);

            $logo = time()."_".$request->file('logo')->getClientOriginalName();
            Storage::putFileAs('company', $request->file('logo'), $logo);

            $company['logo'] = $logo;
        }

        //update 
        $this->companyRepository->update($company, $id);
        return response()
        ->json([
            'success' => true,
            'data' => $company,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $this->companyRepository->delete($id);
        return response()
            ->json([
                'success' => true
            ]);
    }
}
