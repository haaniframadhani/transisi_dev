<?php

namespace Modules\Transisi\Http\Controllers\Api;

use Modules\Transisi\Entities\Employee;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Transisi\Http\Requests\StoreEmployeeRequest;
use Modules\Transisi\Http\Requests\UpdateEmployeeRequest;
use Modules\Transisi\Repositories\EmployeeRepository;

class EmployeeController extends Controller
{
    private $employeeRepository;

    public function __construct()
    {
        $this->employeeRepository = new EmployeeRepository();
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //
        $employees = $this->employeeRepository->fetch();
        return response()
            ->json([
                'success' => true,
                'data' => $employees,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(StoreEmployeeRequest $request)
    {
        $employee = [
            'name' => $request->name,
            'email' => $request->email,
            'company' => $request->company,
            'status' => 1
        ];

        $this->employeeRepository->insert($employee);

        return response()
            ->json([
                'success' => true,
                'data' => $employee,
            ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //

        $employee = $this->employeeRepository->find($id);
        if($employee) {
            return response()->json([
                    'success' => true,
                    'data' => $employee,
                ]);
        } else {
            return response()->json([
                'success' => false,
                'data'    => $employee
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
        $employee = [
            'name' => $request->name,
            'email' => $request->email,
            'company' => $request->company,
            'status' => 1
        ];

        $this->employeeRepository->update($employee, $id);

        return response()
            ->json([
                'success' => true,
                'data' => $employee,
            ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $this->employeeRepository->delete($id);
        return response()
            ->json([
                'success' => true
            ]);
    }
}
