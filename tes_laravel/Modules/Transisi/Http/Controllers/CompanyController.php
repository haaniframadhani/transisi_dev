<?php

namespace Modules\Transisi\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\Transisi\Repositories\CompanyRepository;
use Modules\Transisi\Repositories\EmployeeRepository;

use Modules\Transisi\Http\Requests\StoreCompanyRequest;
use Modules\Transisi\Http\Requests\UpdateCompanyRequest;

class CompanyController extends Controller
{

    private $companyRepository;
    private $employeeRepository;

    public function __construct()
    {
        $this->companyRepository = new CompanyRepository();
        $this->employeeRepository = new EmployeeRepository();
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $companies = $this->companyRepository->fetch();
        return view('transisi::companies/index',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('transisi::companies/create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreCompanyRequest $request)
    {
        // Logo
        $logo = time()."_".$request->file('logo')->getClientOriginalName();
        Storage::putFileAs('company', $request->file('logo'), $logo);

        $company = [
            'name' => $request->name,
            'email' => $request->email,
            'logo' => $logo,
            'website' => $request->website
        ];

        // Store
        $this->companyRepository->insert($company);

        return redirect('/companies')->with('status','Data company baru berhasil ditambahkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $company = $this->companyRepository->find($id);
        $employees = $this->employeeRepository->getByCompany($id);

        return view('transisi::companies/show', compact('company', 'employees'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $company = $this->companyRepository->find($id);
        return view('transisi::companies/edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        $company = [
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
        ];

        // jika update logo baru
        if ($request->hasFile('logo')) {
            $request->validate([
                'logo' => 'file|image|mimes:jpeg,png,jpg|max:2000|dimensions:min_width=100,min_height=100',
            ]);

            $logo = time()."_".$request->file('logo')->getClientOriginalName();
            Storage::putFileAs('company', $request->file('logo'), $logo);

            $company['logo'] = $logo;
        } 
        
        //update 
        $this->companyRepository->update($company, $id);
        return redirect('/companies')->with('status','Data company berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
        $this->companyRepository->delete($id);
        return redirect('/companies')->with('status','Data company berhasil dihapus');
    }
}
