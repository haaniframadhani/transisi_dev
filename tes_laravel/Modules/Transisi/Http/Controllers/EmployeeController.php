<?php

namespace Modules\Transisi\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Transisi\Repositories\EmployeeRepository;
use Modules\Transisi\Entities\Company;
use Modules\Transisi\Http\Requests\StoreEmployeeRequest;
use Modules\Transisi\Http\Requests\UpdateEmployeeRequest;
use Modules\Transisi\Http\Requests\ImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Transisi\Imports\EmployeesImport;


class EmployeeController extends Controller
{

    private $employeeRepository;

    public function __construct()
    {
        $this->employeeRepository = new EmployeeRepository();
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $employees = $this->employeeRepository->fetch();
        return view('transisi::employees/index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('transisi::employees/create');
    }

    // mendapatkan data company untuk dropdown ajax
    public function getCompany(Request $request){
        
        $search = $request->search;

          if($search == ''){
             $companies = Company::limit(5)->get();
          }else{
             $companies = Company::where('name', 'like', '%'.$search.'%')->limit(5)->get();
          }

          $response = array();
          foreach ($companies as $company) {
             $response[] = array(
                  "id"=> $company->id,
                  "text"=> $company->name
             );
          }

          return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreEmployeeRequest $request)
    {
        $employee = [
            'name' => $request->name,
            'email' => $request->email,
            'company' => $request->company,
            'status' => 1

        ];

        $this->employeeRepository->insert($employee);
        return redirect('/employees')->with('status','Data employee baru berhasil ditambahkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $employee = $this->employeeRepository->find($id);
        return view('transisi::employees/show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $employee = $this->employeeRepository->find($id);
        return view('transisi::employees/edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
        $employee = [
            'name' => $request->name,
            'email' => $request->email,
            'company' => $request->company,
            'status' => 1
        ];

        $this->employeeRepository->update($employee, $id);

        return redirect('/employees')->with('status','Data employee berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
        $this->employeeRepository->delete($id);
        return redirect('/employees')->with('status','Data employee berhasil dihapus');
    }



    public function import(ImportRequest $request)
    {
        $file = $request->file('import');
        $filename = rand().$file->getClientOriginalName();

        // upload ke folder file di dalam folder public
        $file->move('file', $filename);
 
        // import data
        Excel::import(new EmployeesImport, public_path('/file/'.$filename));

        // alihkan halaman kembali
        return redirect('/employees')->with('status','Data employee berhasil diimport!');
    }
}
