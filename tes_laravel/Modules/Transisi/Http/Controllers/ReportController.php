<?php

namespace Modules\Transisi\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use PDF;
use Modules\Transisi\Exports\EmployeesExport;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Transisi\Repositories\EmployeeRepository;
use Modules\Transisi\Repositories\CompanyRepository;

class ReportController extends Controller
{

    public function employees($company_id)
    {
        $employeeRepository = new EmployeeRepository();
        $employees = $employeeRepository->getByCompany($company_id);

        $companyRepository = new CompanyRepository();
        $company = $companyRepository->find($company_id);

        $pdf = PDF::loadView('transisi::reports/list', ['employees' => $employees, 'company' => $company]);
        return $pdf->download('employees of '.$company->name.'.pdf');
    }

    public function exportExcelEmployees($company_id)
    {
        // echo $company_id;
        return (new EmployeesExport($company_id))->download('employees.xlsx');
    }
}

