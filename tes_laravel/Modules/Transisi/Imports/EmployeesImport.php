<?php

namespace Modules\Transisi\Imports;

use Modules\Transisi\Entities\Employee;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class EmployeesImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $employee = new Employee();
        
        foreach ($rows as $row) 
        {
            $data = [
                'name' => $row[0],
                'email' => $row[1],
                'company' => $row[2],
                'status' => $row[3],
            ];
            $employee->insert($data);
        }



    }
}
