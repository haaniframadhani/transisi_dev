<?php
namespace Modules\Transisi\Repositories;

use Modules\Transisi\Entities\Company;

class CompanyRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Company();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function fetch()
    {
        return $this->model->query()->paginate(5);
    }

    public function insert(array $params)
    {
        $this->model->create($params);
    }

    public function update(array $params, $id)
    {
        $this->model->where('id', $id)->update($params);
    }

    public function delete($id)
    {
        $this->model->destroy($id);
    }
}
