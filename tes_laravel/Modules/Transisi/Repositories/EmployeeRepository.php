<?php
namespace Modules\Transisi\Repositories;

use Modules\Transisi\Entities\Employee;

class EmployeeRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Employee();
    }

    public function find($id)
    {
        return $this->model->select('employees.id as id', 'employees.name as name', 'employees.email as email', 'companies.name as company', 'status')
                            ->join('companies','companies.id','=','employees.company')
                            ->where('employees.id', $id)
                            ->first();

    }

    public function fetch()
    {
        return $this->model->select('employees.id as id', 'employees.name as name', 'employees.email as email', 'companies.name as company', 'status')
                            ->join('companies','companies.id','=','employees.company')
                            ->paginate(5);
    }

    public function getByCompany($company)
    {
        return $this->model->select('employees.id as id', 'employees.name as name', 'employees.email as email', 'companies.name as company', 'status')
                            ->join('companies','companies.id','=','employees.company')
                            ->where('employees.company', $company)
                            ->get();
    }

    public function insert(array $params)
    {
        $employee = new Employee();
        $employee->name = $params['name'];
        $employee->email = $params['email'];
        $employee->company = $params['company'];
        $employee->status = $params['status'];
        $employee->save();
    }

    public function update(array $params, $id)
    {
        $this->model->where('id', $id)->update($params);
    }

    public function delete($id)
    {
        $this->model->destroy($id);
    }
}
