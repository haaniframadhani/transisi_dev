@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Companies</h3>
    <div class="dropdown-divider"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="/companies" enctype="multipart/form-data">
                        @csrf
                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name of company">
                      </div>
                        @error('name')
                          <div class="form-group small text-danger">{{ $message }}</div>
                        @enderror
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                      </div>
                        @error('email')
                          <div class="form-group small text-danger">{{ $message }}</div>
                        @enderror
                      <div class="form-group">
                        <label for="website">Website</label>
                        <input type="text" class="form-control" id="website" name="website" placeholder="https://example.com">
                      </div>
                        @error('website')
                          <div class="form-group small text-danger">{{ $message }}</div>
                        @enderror
                      <div class="form-group">
                        <label for="logo">Logo</label>
                        <input type="file" class="form-control-file" id="logo" name="logo">
                      </div>
                        @error('logo')
                          <div class="form-group small text-danger">{{ $message }}</div>
                        @enderror

                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>



</div>
@endsection
