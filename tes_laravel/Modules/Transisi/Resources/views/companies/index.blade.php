@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-sm">
        <p class="h3 p-2 m-0">Companies</p>
      </div> 
      <div class="col-sm">
        <a href="companies/create" class="btn btn-primary float-right">
          Create new
        </a>
      </div><!-- /.col -->
    </div>

    @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
    @endif

    <div class="row">

        <div class="col-md-12">

            <div class="card">

                <div class="card-body">
                    <ul class="list-group">
                        @foreach ($companies as $company)

                        @php $path = Storage::url($company->logo); @endphp

                          <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-2">
                                    <img src="{{ url($path) }}" alt="logo company" width="100px">
                                </div>
                                <div class="col-sm-7">

                                    <h5>{{ $company->name }}</h5>
                                </div>
                                
                                <div class="col-sm-3">
                                    <a href="/companies/{{$company->id}}" class="btn btn-info text-white">Detail</a>
                                    <a href="/companies/{{$company->id}}/edit" class="btn btn-light">Edit</a> 
                                    <form action="/companies/{{$company->id}}" method="post" class="d-inline">
                                      @method('delete')
                                      @csrf
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </div>
                            </div>
                          </li>
                        @endforeach
                        <div class="row justify-content-center">    
                            <?= $companies->links() ?>
                        </div>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
