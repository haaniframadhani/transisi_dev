@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col-10">
        <div class="card">

          <?php $path = Storage::url($company->logo); ?>
          <img class="card-img-top" src="<?= url($path) ?>" alt="Card image cap">
          <div class="card-body text-center">
            <h3 class="card-title"><?= $company->name ?></h3>
            <p class="card-text"><?= $company->email ?></p>
            <p class="card-text"><?= $company->website ?></p>
            <!-- <a href="/employees/{{ $company->id}}" class="btn btn-primary">Lihat Employee</a> -->
            <div class="p-2">  
                
                <h5>Employees list : </h5>
                <div class="row justify-content-center">
                  <a href="/report/excel/employees/{{$company->id}}" class="btn btn-success m-1">Export excel</a>
                  <a href="/report/employees/{{$company->id}}" class="btn btn-success m-1">Export PDF</a>
                </div>
              <table class="table table-hover table-bordered">
                <tr>
                  <th>No.</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Tindakan</th>
                </tr>

              <?php $no=1; ?>
              <?php foreach ($employees as $employee) : ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $employee->name ?></td>
                  <td><?= $employee->email ?></td>
                  <td><?= $employee->status ?></td>
                  <td>
                    <a href="/employees/{{$employee->id}}" class="btn btn-info text-white">Detail</a>
                    <a href="/employees/{{$employee->id}}/edit" class="btn btn-light">Edit</a> 
                    <form action="/employees/{{$employee->id}}" method="post" class="d-inline">
                      @method('delete')
                      @csrf
                      <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                  </td>
                </tr>  
              <?php endforeach ?>

              </table>
            
            </div>

          </div>
        </div>
      </div> 
    </div>
</div>
@endsection
