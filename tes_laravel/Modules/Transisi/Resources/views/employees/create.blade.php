@extends('layouts.app2')

@section('content')
<div class="container">
    <h3>New Employee</h3>
    <div class="dropdown-divider"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="/employees">
                        @csrf
                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        @error('name')
                          <div class="form-group small text-danger my-1">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                        @error('email')
                          <div class="form-group small text-danger my-1">{{ $message }}</div>
                        @enderror
                      </div>

                      <div class="form-group">
                        <label for="company">Company</label>
                        <select class="form-control" id='company' name="company">
                          <option value=0>-- Select company --</option>
                        </select>
                        @error('company')
                          <div class="form-group small text-danger my-1">{{ $message }}</div>
                        @enderror
                      </div>

                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    // CSRF Token
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function(){

      $( "#company" ).select2({
        ajax: { 
          url: "{{ route('employees.getCompany') }}",
          type: "post",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              _token: CSRF_TOKEN,
              search: params.term // search term
            };
          },
          processResults: function (response) {
            return {
              results: response
            };
          },
          cache: true
        }

      });

    });
</script>

@endsection