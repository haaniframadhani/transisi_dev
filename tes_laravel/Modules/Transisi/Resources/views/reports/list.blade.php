<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Report List</title>
</head>
<body>

	<div class="container p-3">
		<h3>List Employee di <?= $company->name ?></h3>
		<table border="1" cellspacing="0" width="100%" cellpadding="5"> 
			<tr>
				<th width="5%">No.</th>
				<th width="50%">Name</th>
				<th width="30%">Email</th>
				<th>Status</th>
			</tr>
			<?php $no=1; ?>
			<?php foreach ($employees as $employee) : ?>
				<tr>
					<td><?= $no++ ?></td>
					<td><?= $employee->name ?></td>
					<td><?= $employee->email ?></td>
					<td><?= $employee->status ?></td>
				</tr>
			<?php endforeach; ?>
		</table>
	</div>
</body>
</html>