<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/transisi', function (Request $request) {
    return $request->user();
});



Route::get('/company', 'Api\CompanyController@index');
Route::get('/company/{id}', 'Api\CompanyController@show');
Route::post('/company', 'Api\CompanyController@store');
Route::put('/company/{id}', 'Api\CompanyController@update');
Route::delete('/company/{id}', 'Api\CompanyController@destroy');

Route::get('/employee', 'Api\EmployeeController@index');
Route::get('/employee/{id}', 'Api\EmployeeController@show');
Route::post('/employee', 'Api\EmployeeController@store');
Route::put('/employee/{id}', 'Api\EmployeeController@update');
Route::delete('/employee/{id}', 'Api\EmployeeController@destroy');