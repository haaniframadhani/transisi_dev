<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('transisi')->group(function() {
    Route::get('/', 'TransisiController@index');
});



Route::middleware('auth')->group(function() {
    Route::resource('companies', 'CompanyController');
    Route::resource('employees', 'EmployeeController');

    Route::post('employees/getCompany', 'EmployeeController@getCompany')->name('employees.getCompany');
    Route::post('employees/import', 'EmployeeController@import');

    Route::get('report/employees/{company_id}', 'ReportController@employees');
    Route::get('report/excel/employees/{company_id}', 'ReportController@exportExcelEmployees');
});
