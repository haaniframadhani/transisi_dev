<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    //
    protected $fillable = [
        'name', 'email', 'company_id'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function insert($data)
    {
        DB::table('employees')->insert($data);
    }
}
