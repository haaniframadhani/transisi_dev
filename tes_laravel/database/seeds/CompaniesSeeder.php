<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //////
        $faker = Faker::create('id_ID');
 
        for($i = 1; $i <= 7; $i++){
 
              // insert data ke table employees menggunakan Faker
            DB::table('companies')->insert([
                'name' => $faker->company,
                'email' => $faker->unique()->email,
                'logo' => '1633674382_sibuk kerja.jpg',
                'website' => $faker->url
            ]);
        }
    }
}
