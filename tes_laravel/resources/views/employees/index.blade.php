@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-sm">
        <p class="h3 p-2 m-0">Employee</p>
      </div> 
      <div class="col-sm">
        <a href="/employees/create" class="btn btn-primary float-right m-1">
          Create new
        </a>
        <a href ="#" class="btn btn-primary float-right m-1" data-toggle="modal" data-target="#importModal">
          Import
        </a>

        <!-- Modal -->
        <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Data Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

                <form method="post" action="/employees/import" enctype="multipart/form-data">
                  @csrf
                  <div class="modal-body">
                    <div>
                      <label class="font-weight-bold">Import (<span class="text-danger">*</span>csv, xls, xlsx)</label>
                    </div>
                    <input type="file" name="import">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
            </div>
          </div>
        </div>
      </div><!-- /.col -->
    </div>

    @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
    @endif

    <div class="row">

        <div class="col-md-12">

            <div class="card">

                <div class="card-body">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Company</th>
                            <th>Status</th>
                            <th>Tindakan</th>
                        </tr>
                        <?php foreach ($employees as $employee) : ?>
                            <tr>
                                <td><?= $employee->name ?></td>
                                <td><?= $employee->email ?></td>
                                <td><?= $employee->company ?></td>
                                <td><?= $employee->status ?></td>
                                <td>
                                    <a href="/employees/{{$employee->id}}" class="btn btn-info text-white">Detail</a>
                                    <a href="/employees/{{$employee->id}}/edit" class="btn btn-light">Edit</a> 
                                    <form action="/employees/{{$employee->id}}" method="post" class="d-inline">
                                      @method('delete')
                                      @csrf
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    <div class="row justify-content-center">    
                        <?= $employees->links() ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
