@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col-sm-8">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Name : <?= $employee->name ?></h5>
            <p class="card-text">Email : <?= $employee->email ?></p>
            <p class="card-text">Company : <?= $employee->company ?></p>
            <p class="card-text">Status : <?= $employee->status ?></p>
          </div>
        </div>
      </div> 
    </div>
</div>
@endsection
